import os
import glob
import json

path = os.getcwd()  # current working directory
counter_files_checked = 0  # counting the number of files checked
counter_targets_found = 0  # counting the number of targets found

with open('kollektivavtal-not-annotated.txt', 'w') as g:  # open txt file to write filenames to
    for filename in glob.glob(os.path.join(path, '*.json')):  # checking all json files
        counter_files_checked += 1  # updating the number of files checked
        with open(filename, encoding='utf-8', mode='r') as f:
            data = f.read()  # read data
            ad_text = json.loads(data)["text"]  # place of ad texts in json files
            annotations = json.loads(data)["annotations"]  # place of annotations in json files

            annotated = False  # initialize variable

            # If the word "kollektivavtal" is in the job ad text
            if "kollektivavtal" in ad_text.lower():

                # If it was annotated, set to True
                for concept in annotations:
                    if concept["preferred-label"] == "Kollektivavtal":
                        annotated = True

                # If False (it was not annotated)
                if annotated is False:
                    targets = os.path.basename(filename)  # save filename
                    counter_targets_found += 1  # update the number of targets found
                    print(targets)  # print filenames
                    g.write(targets)  # write to file
                    g.write("\n")  # add new line

print(f'{counter_targets_found} targets were found in {counter_files_checked} files checked.')
